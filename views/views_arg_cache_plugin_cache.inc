<?php

/**
 * Simple caching based on arguments and display_id
 * 
 * Makes it easier to only purge Views caches for specific argument(s)
 * 
 * This cache can only be purged from the code.
 * 
 * @see
 *   views_arg_cache_flush()
 */
class views_arg_cache_plugin_cache extends views_plugin_cache {
  /**
   * What table to store data in.
   */
  var $table = VIEWS_ARG_CACHE_TABLE;
  
  /**
   * Override of parent::get_results_key()
   */
  function get_results_key() {
    global $user;

    // Taken from parent::get_results_key()
    if (!isset($this->_results_key)) {
      $build_info = $this->view->build_info;

      $query_plugin = $this->view->display_handler->get_plugin('query');

      foreach (array('query','count_query') as $index) {
        // If the default query back-end is used generate SQL query strings from
        // the query objects.
        if ($build_info[$index] instanceof SelectQueryInterface) {
          $query = clone $build_info[$index];
          $query->preExecute();
          $build_info[$index] = (string)$query;
        }
      }
      $key_data = array(
        'build_info' => $build_info,
        'roles' => array_keys($user->roles),
        'super-user' => $user->uid == 1, // special caching for super user.
        'language' => $GLOBALS['language']->language,
      );
      foreach (array('exposed_info', 'page', 'sort', 'order') as $key) {
        if (isset($_GET[$key])) {
          $key_data[$key] = $_GET[$key];
        }
      }
      
      $this->_results_key = _views_arg_cache_get_key($this->view->name, $this->display->id, $this->view->args) . ':results:' . md5(serialize($key_data));
    }
    
    return $this->_results_key;
  }

  /**
   * Override of parent::get_output_key()
   */
  function get_output_key() {
    global $user;
    // Taken from parent::get_output_key()
    if (!isset($this->_output_key)) {
      $key_data = array(
        'result' => $this->view->result,
        'roles' => array_keys($user->roles),
        'super-user' => $user->uid == 1, // special caching for super user.
        'theme' => $GLOBALS['theme'],
        'language' => $GLOBALS['language']->language,
      );

      $this->_output_key = _views_arg_cache_get_key($this->view->name, $this->display->id, $this->view->args) . ':output:' . md5(serialize($key_data));
    }
    
    return $this->_output_key;
  }
  
  /**
   * Clear out cached data for a view.
   */
  function cache_flush() {
    views_arg_cache_flush($this->view->name, $this->display->id, $this->view->args);
  }
  
  function summary_title() {
    return t('Argument cache');
  }
}
